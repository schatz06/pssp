import sys
import numpy as np
import os
import os.path
from os import path
import hdf5storage

############################## FILES AND FOLDERS #######################################
folder = sys.argv[1] + '/content/' + sys.argv[1] + '/'
fold_file = sys.argv[2]
output_file = sys.argv[3]
# print(folder)
# print(fold_file)
# print(output_file)
train = open(fold_file,"r")
################## MAIN PROGRAM ###########################
target_name = 1 # target line that has the name of the protein
target_secondary = 3 # target line that has the primary structure
count = 1
new = 0
while True:
    # Get next line from file
    if count % 100 == 0:
        print(count)
    line = train.readline()
    if not line:
        break
    if count == target_name:
        line = line.replace("\n", "")  # remove the newline character in the end of the string
        line = line.replace(" ", "")
        line = line.replace(">", "")
        name = folder + line + ".npy"
        target_name += 3
        # print(name)
        if path.exists(name):
            embedding = np.load(name)
            a = np.array(embedding)
            #a = a.flatten()
            if count == 1:
                new = a
            else:
                new = np.vstack([new, a])
        else:
            print("Protein not exists: ",name)
    if count == target_secondary:
        line = line.replace("\n", "")  # remove the newline character in the end of the string
        temp = np.zeros((len(line)), dtype=np.short)
        for j in range(0, len(line)):
            if line[j] == 'C':
                temp[j] = 0
            if line[j] == 'H':
                temp[j] = 1
            if line[j] == 'E':
                temp[j] = 2
        if count == 3:
            y = temp
        else:
            y = np.append(y, temp, axis=0)
        target_secondary += 3
        # scipy.io.savemat(f, {line:arr})
    count += 1
hdf5storage.savemat(output_file, {'x': new})
y = y.reshape(-1,1)
#print(y.shape)
hdf5storage.savemat(output_file, {'y': y})