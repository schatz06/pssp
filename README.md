# PSSP_Project

<details><summary>Table of Contents</summary><p>

- [Installation](#installation)
- [Usage](#usage)
    - [Prepare data](#prepare-data)
    - [Get notebook and data](#get-notebook-and-data)
    - [Arguments](#arguments)
    - [Filtering techniques](#filtering-techniques)
- [Bugs](#bugs)
- [License](#license)
</p></details><p></p>

This project (PSSP) is an attempt to solve the Protein Secondary Structure Prediction (PSSP) problem using continuous input vector representation through Embeddings from Language Models (BERT) [1] with Convolutional Neural Networks (CNN) and the Hessian Free Optimisation (HFO) algorithm. The embeddings we are using are based on the works by Elnaggar et al. [2] and Dallago et al. [3], while for CNN/HFO, the implementation provided by Wang et al. [4] was used.

[1] J. Devlin, M.-W. Chang, K. Lee and K. Toutanova, "BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding," arXiv preprint arXiv: 1810.04805v2, 2019.

[2] A. Elnaggar, M. Heinzinger, C. Dallago, G. Rehawi, Y. Wang, L. Jones, T. Gibbs, T. Feher, C. Angerer, M. Steinegger, D. Bhowmik, and B. Rost., "ProtTrans: Toward Understanding the Language of Life Through Self-Supervised Learning," IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 44, no. 10, pp. 7112-7127, 2022.

[3] C. Dallago, K. Schütze, M. Heinzinger, T. Olenyi, M. Littmann, A. X. Lu, K. K. Yang, S. Min, S. Yoon, J. T. Morton, and B. Rost, "Learned Embeddings from Deep Learning to Visualize and Predict Protein Sets," Current Protocols, vol. 1, no. 5, Article e113, 2021.

[4] C. Wang, K. Tan and C.-J. Lin, "Newton Methods for convolutional neural networks," ACM Transactions on Intelligent Systems and Technology, vol. 11, no. 2. Article 19, 2020."

[![License](http://img.shields.io/:license-GNU-green.svg)](https://gitlab.com/schatz06/pssp/-/blob/master/LICENSE)
# Installation
It is highly recommended to use Colaboratory ([Colab](https://colab.research.google.com/notebooks/welcome.ipynb)) to run the notebooks, because it allows to write and execute Python code in a browser with:
* Zero configuration required
* Free access to GPUs and TPUs
* Most libraries pre-installed
* Only one requirement, a google account
* Most common Machine Learning frameworks pre-installed and ready to use

### Requirements
- Python (>= 3.6)
- Jupyter (>= 5.2.2)
- NumPy (>= 1.13.3)
- TensorFlow (>= 2.2.0)
- SciPy (>= 0.19.1)
- Pandas (>= 1.0.3)
- hdf5storage (>= 0.1.15)
- Scikit-learn (>= 0.23.0)
- tqdm (>= 4.60.0)

### Installing Jupyter Notebook 
First, ensure that you have the latest pip; older versions may have trouble with some dependencies:
```sh
pip3 install --upgrade pip
```

#### conda (Windows/MacOS/Linux)
If you use conda, you can install Jupyter Notebook with:
```sh
conda install -c conda-forge notebook
```

#### pip (Windows/MacOS/Linux)
If you use pip, you can install Jupyter Notebook with:
```sh
pip install notebook
```

#### run
To run the notebook, use the following command at the Terminal (Mac/Linux) or Command Prompt (Windows):
```sh
jupyter notebook
```

### Installing Scikit-learn 

#### conda (Windows/MacOS/Linux)
If you use conda, you can install it with:
```sh
conda install scikit-learn
```

In order to check your installation you can use:
```sh
conda list scikit-learn # to see which scikit-learn version is installed
conda list # to see all packages installed in the active conda environment
python3 -c "import sklearn; sklearn.show_versions()"
```

#### pip (Windows/MacOS)
If you use pip, you can install it with:
```sh
pip install -U scikit-learn
```

In order to check your installation you can use:
```sh
python3 -m pip show scikit-learn # to see which version and where scikit-learn is installed
python3 -m pip freeze # to see all packages installed in the active virtualenv
python3 -c "import sklearn; sklearn.show_versions()"
```

# Usage
### Prepare data 
>Note: If you want to train your model with the already extracted CB513 dataset matlab files skip the rest steps and move to the **Get notebook** section

In order to prepare your data follow the steps below:
1. Download the [extract_protbert_embeddings.ipynb](https://gitlab.com/schatz06/pssp/-/blob/master/data_preprocessing/extract_protbert_embeddings.ipynb)

2. Select and download which dataset you would like to extract embeddings: [CASP13](https://gitlab.com/schatz06/pssp/-/blob/master/Datasets/CASP13) , [CB513](https://gitlab.com/schatz06/pssp/-/blob/master/Datasets/CB513) , [PISCES](https://gitlab.com/schatz06/pssp/-/blob/master/Datasets/PISCES)
    
3. Open [Colab](https://colab.research.google.com/notebooks/welcome.ipynb) and sign in to your Google account. If you do not have a Google account, you can create one [here](https://accounts.google.com/signup/v2/webcreateaccount?hl=en&flowName=GlifWebSignIn&flowEntry=SignUp).

4. Go to _File > Upload notebook > Choose file_ and browse to find the downloaded notebook [extract_protbert_embeddings.ipynb](https://gitlab.com/schatz06/pssp/-/blob/master/data_preprocessing/extract_protbert_embeddings.ipynb) If you have already uploaded the notebook to Colab you can open it with _File > Open notebook_ and choose **extract_protbert_embeddings.ipynb**. 

5. Also upload the dataset of your choice and assign the name of the dataset into the dataset variable that is at the start of the notebook. 

6. Run the notebook, the embeddings are going to be in a zip named (dataset_name)_Protbert.zip, download the zip file to continue to **step 7**.

7. Download the [extract_folds.py](https://gitlab.com/schatz06/pssp/-/blob/master/data_preprocessing/extract_folds.py) python script. 

8. Put in the same directory the **extract_folds.py** program, the zip file with the embeddings and the corresponding dataset that you used to extract the embeddings, in order to create the .mat file to be used as input to the Convolutional Neural Network. 

9. Unzip the file that contains the embeddings and execute the python file.
```sh
# example of exectuion
python3 extract_folds.py <unzip_folder_name> <protein_file.txt> <output.mat>

python3 extract_folds.py CASP13_sorted_ProtBert  CASP13_sorted.txt casp13_protbert.mat
```
>Note: filename of .mat files should be in the form of:\
    `CASP13:`casp13_protbert.mat\
    `CB513:` cb513_protbert_testSet{number}.mat e.g. cb513_protbert_testSet0.mat\
             cb513_protbert_trainSet{number}.mat e.g. cb513_protbert_trainSet0.mat\
    `PISCES:`pisces_protbert_testSet{number}.mat e.g. pisces_protbert_testSet0.mat\
             pisces_protbert_trainSet{number}.mat e.g. pisces_protbert_trainSet0.mat
### Get notebook and data 
In order to be able to run the notebook first you need to go through the **Prepare Data** section, to extract the train,validation and independent test matlab files.
To perform experiments using the embeddings extracted above, follow the steps below:

0. Download the [CNN_HFO_v2.ipynb](https://gitlab.com/schatz06/pssp/-/blob/master/Neural%20Network/CNN_HFO_v2.ipynb).

1. Open [Colab](https://colab.research.google.com/notebooks/welcome.ipynb) and sign in to your Google account. If you do not have a Google account, you can create one [here](https://accounts.google.com/signup/v2/webcreateaccount?hl=en&flowName=GlifWebSignIn&flowEntry=SignUp).

2. Go to _File > Upload notebook > Choose file_ and browse to find the downloaded notebook file [CNN_HFO_v2.ipynb](https://gitlab.com/schatz06/pssp/-/blob/master/Neural%20Network/CNN_HFO_v2.ipynb). If you have already uploaded the notebook to Colab you can open it with _File > Open notebook_ and choose **CNN_HFO_v2.ipynb**. 

3. Once the notebook is loaded, go to _Runtime > Change runtime type_ and from the dropdown menu, under **Hardware accelerator**, choose **GPU** and click **Save**.

4. Now you can begin the experiments with the PSSP datasets. All you have to do is to choose the **fold** (dataset fold number), the **dataset** ("CB513" or "PISCES") and the **USE_HFO** (default True) to set the optimiser.

5. To train the model go to _Runtime > Run all_ or click on the first cell and use **Shift + Enter** to execute each cell one by one.

6. The hyper parameters of the model can be modified in the cell under **Set Train Arguments** section. 

## Train
Instead of feeding the entire subsampled data into GPU memory to evaluate sub-sampled Gauss-Newton matrix vector product, the samples are divided into segments of size bsize and accumulate results to avoid the out-of-memory issue. For the core operation of Gauss-Newton matrix-vector products, the Tensorflow's vector-Jacobian products are used; the implementation details can be found in this [document](https://www.csie.ntu.edu.tw/~cjlin/papers/cnn/Calculating_Gauss_Newton_Matrix_Vector_product_by_Vector_Jacobian_Products.pdf).

The program gets the validation accuracy at each iteration and returns the best model, if a validation set is provided. Otherwise, the model obtained at the last iteration is returned. The read_data function under the **Utilities** section reads the MATLAB file, performs data normalization and reshapes the input data. Please make sure the input datasets are converted into MATLAB format before they are fed to the network.

### Arguments
Available options and default hyper parameters:

#### General 
1. --optim: the optimization method used for training CNN. (NewtonCG or SGD)
```
Default: --optim NewtonCG
```

2. --net: network configuration (CNN\_4layers, CNN\_7layers, VGG11, VGG13, VGG16, and VGG19)
```
Default: --net CNN_4layers
```

3. --train\_set & --val\_set: provide the address of .mat file for training or validation (optional).
```
Default: None
```

4. --model: save model to a file
```
Default: --model ./saved_model/model.ckpt
```

5. --loss: which loss function to use: MSELoss or CrossEntropy
```
Default: --loss MSELoss
```

6. --bsize: Split data into segments of size bsize so that each segment can fit into memory for evaluating Gv, stochastic gradient and global gardient. If you encounter Out of Memory (OOM) during training, you may decrease the --bsize parameter to an appropriate value.
```
Default: --bsize 1024
```

7. --log: saving log to a file
```
Default: --log ./running_log/logger.log
```

8. --screen\_log\_only: if specified, log printed on screen only but not stored to the log file
```
Default: --screen_log_only
```

9. --C: regularization parameter. Regularization term = 1/(2C × num\_data) × L2\_norm(weight)^2
```
Default: --C 0.01
```

10. --dim: input dimension of data. Shape must be: height width num_channels
```
Default: --dim 32 32 3
```

11. --seed: specify random seed to make results deterministic. If no random seeds are given, a different result is produced after each run.

#### Newton Method
1. --GNsize: number of samples used in the subsampled Gauss-Newton matrix.
```
Default: --GNsize 4096
```

2. --iter_max: the maximal number of Newton iterations
```
Default: --iter_max 100
```

3. --xi: the tolerance in the relative stopping condition for CG
```
Default: --xi 0.1
```

4. --drop: the drop constants for the LM method
```
Default: --drop 2/3
```

5. --boost: the boost constants for the LM method
```
Default: --boost 3/2
```

6. --eta: the parameter for the line search stopping condition
```
Default: --eta 0.0001
```

7. --CGmax: the maximal number of CG iterations
```
Default: --CGmax 250
```

8. --lambda: the initial lambda for the LM method
```
Default: --lambda 1
```

#### SGD
1. --decay: learning rate decay over each mini-batch update.
```
Default: --decay 0
```

2. --momentum: SGD + momentum
```
Default: --momentum 0
```

3. --epoch_max: number of training epoch
```
Default: --epoch_max 500
```

4. --lr: learning rate
```
Default: --lr 0.01
```

## Test 
### Arguments 
Available options and default hyper parameters:

1. --model: address of the saved model from training
```
Default: --model ./saved_model/model.ckpt
```

2. --dim: input dimension of data. Shape must be: height width num_channels
```
Default: --dim 32 32 3
```

3. --train\_set & --test\_set: provide the directory of .mat file for train and test to get predictions.
```
Default: None
```

4. --bsize: Split data into segments of size bsize so that each segment can fit into memory for stochastic gradient and global gradient. If you encounter Out of Memory (OOM) during training, you may decrease the --bsize parameter to an appropriate value.
```
Default: --bsize 1024
```

5. --loss: which loss function to use: MSELoss or CrossEntropy
```
Default: --loss MSELoss
```

## Filtering techniques
The filtering techniques can be found on [Panayiotis Leontiou gitlab repository](https://gitlab.com/perf.ai/pssp_article/-/tree/master/) 

# Bugs
No known bugs

# License
[![License](http://img.shields.io/:license-GNU-green.svg)](https://gitlab.com/perf.ai/pssp_article/-/blob/master/LICENSE)

- This project is under [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/).
